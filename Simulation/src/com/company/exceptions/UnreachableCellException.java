package com.company.exceptions;

/**
 * Created by Tisov-VV on 22.10.2016.
 * Исключение, говорящее об обращении к несуществующей клетке
 */
public class UnreachableCellException extends ActionMessageException {
    public UnreachableCellException() {
    }

    public UnreachableCellException(String s) {
        super(s);
    }

    public UnreachableCellException(String s, Throwable throwable) {
        super(s, throwable);
    }

    public UnreachableCellException(Throwable throwable) {
        super(throwable);
    }

    public UnreachableCellException(String s, Throwable throwable, boolean b, boolean b1) {
        super(s, throwable, b, b1);
    }
}
