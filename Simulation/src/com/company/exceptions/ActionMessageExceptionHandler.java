package com.company.exceptions;

import com.company.entities.ActiveEntity;
import com.company.processor.messages.requests.ActionRequest;
import com.company.processor.messages.responses.ActionResponse;
import com.company.processor.messages.responses.ActionResponseState;

public class ActionMessageExceptionHandler<T extends ActiveEntity> implements ExceptionHandler<T> {

    @Override
    public void handle(Exception e, ActionRequest request, T activeEntity) {
        activeEntity.listenResponse(new ActionResponse(ActionResponseState.ERROR, request.getId()));
        e.printStackTrace();
    }
}
