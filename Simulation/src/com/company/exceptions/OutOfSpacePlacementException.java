package com.company.exceptions;

/**
 * Created by Tisov-VV on 22.10.2016.
 * Исключение, говорящее о попытке расположить объект за пределами нашего пространства
 */
public class OutOfSpacePlacementException extends SimulationException {
    public OutOfSpacePlacementException() {
    }

    public OutOfSpacePlacementException(String s) {
        super(s);
    }

    public OutOfSpacePlacementException(String s, Throwable throwable) {
        super(s, throwable);
    }

    public OutOfSpacePlacementException(Throwable throwable) {
        super(throwable);
    }

    public OutOfSpacePlacementException(String s, Throwable throwable, boolean b, boolean b1) {
        super(s, throwable, b, b1);
    }
}
