package com.company.exceptions;

import com.company.entities.ActiveEntity;
import com.company.processor.messages.requests.ActionRequest;

public interface ExceptionHandler <T extends ActiveEntity> {

    void handle(Exception e, ActionRequest request, T activeEntity);

}
