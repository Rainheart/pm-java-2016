package com.company.exceptions;

/**
 * Created by Tisov-VV on 22.10.2016.
 * Базовый класс для исключенй нашего проекта
 */
public abstract class SimulationException extends Exception {
    public SimulationException() {
    }

    public SimulationException(String s) {
        super(s);
    }

    public SimulationException(String s, Throwable throwable) {
        super(s, throwable);
    }

    public SimulationException(Throwable throwable) {
        super(throwable);
    }

    public SimulationException(String s, Throwable throwable, boolean b, boolean b1) {
        super(s, throwable, b, b1);
    }
}
