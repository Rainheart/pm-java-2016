package com.company.exceptions;

/**
 * Created by Tisov-VV on 22.10.2016.
 * Исключение для фиксации неизвестного запроса на действие
 */
public class UnknownActionRequestException extends ActionMessageException {
    public UnknownActionRequestException() {
    }

    public UnknownActionRequestException(String s) {
        super(s);
    }

    public UnknownActionRequestException(String s, Throwable throwable) {
        super(s, throwable);
    }

    public UnknownActionRequestException(Throwable throwable) {
        super(throwable);
    }

    public UnknownActionRequestException(String s, Throwable throwable, boolean b, boolean b1) {
        super(s, throwable, b, b1);
    }
}
