package com.company.exceptions;

import com.company.entities.ActiveEntity;

import java.util.HashMap;
import java.util.Map;

public class ExceptionHandlersFactory {

    private static Map<Class<? extends Exception>, ExceptionHandler> factoryMap = new HashMap<>();

    static {
        factoryMap.put(EntityDieException.class, new EntityDieExceptionHandler());
        factoryMap.put(UnreachableCellException.class, new ActionMessageExceptionHandler());
        factoryMap.put(ActionMessageException.class, new ActionMessageExceptionHandler());
    }

    public static ExceptionHandler<ActiveEntity> getHandler(Exception exception) {
        ExceptionHandler handler = factoryMap.get(exception.getClass());
        return handler;
    }
}
