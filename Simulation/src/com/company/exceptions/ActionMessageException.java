package com.company.exceptions;

/**
 * Created by Tisov-VV on 22.10.2016.
 * Исключения, возникшие при работе с запросами на действие
 */
public class ActionMessageException extends SimulationException {
    public ActionMessageException() {
    }

    public ActionMessageException(String s) {
        super(s);
    }

    public ActionMessageException(String s, Throwable throwable) {
        super(s, throwable);
    }

    public ActionMessageException(Throwable throwable) {
        super(throwable);
    }

    public ActionMessageException(String s, Throwable throwable, boolean b, boolean b1) {
        super(s, throwable, b, b1);
    }
}
