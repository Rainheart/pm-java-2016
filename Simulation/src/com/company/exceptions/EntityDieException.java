package com.company.exceptions;

import com.company.entities.MortalWanderingOne;

public class EntityDieException extends ActionMessageException {

    private MortalWanderingOne entity;

    public EntityDieException() {}

    public EntityDieException(MortalWanderingOne entity) {
        this.entity = entity;
    }

    public MortalWanderingOne getEntity() {
        return entity;
    }
}
