package com.company.exceptions;

import com.company.Main;
import com.company.entities.MortalWanderingOne;
import com.company.processor.messages.requests.ActionRequest;
import com.company.processor.messages.responses.ActionResponse;
import com.company.processor.messages.responses.ActionResponseState;

public class EntityDieExceptionHandler extends ActionMessageExceptionHandler<MortalWanderingOne> {

    @Override
    public void handle(Exception e, ActionRequest request, MortalWanderingOne activeEntity) {
        //Смертный странник умер
        activeEntity.die();
        Main.getProcessor().getDeadEntities().add(activeEntity);
        activeEntity.listenResponse(new ActionResponse(ActionResponseState.DIE, request.getId()));
        System.out.println(activeEntity.getName() + " умер");
        e.printStackTrace();
    }
}
