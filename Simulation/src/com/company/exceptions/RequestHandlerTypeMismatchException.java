package com.company.exceptions;

/**
 * Created by Tisov-VV on 22.10.2016.
 * Исключение, говорящее о том, что в обработчик попал запрос неизвестного типа
 */
public class RequestHandlerTypeMismatchException extends ActionMessageException {
    public RequestHandlerTypeMismatchException() {
    }

    public RequestHandlerTypeMismatchException(String s) {
        super(s);
    }

    public RequestHandlerTypeMismatchException(String s, Throwable throwable) {
        super(s, throwable);
    }

    public RequestHandlerTypeMismatchException(Throwable throwable) {
        super(throwable);
    }

    public RequestHandlerTypeMismatchException(String s, Throwable throwable, boolean b, boolean b1) {
        super(s, throwable, b, b1);
    }
}
