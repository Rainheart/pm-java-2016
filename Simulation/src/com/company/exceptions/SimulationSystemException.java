package com.company.exceptions;

/**
 * Created by Tisov-VV on 23.10.2016.
 * Системное исключение проекта, фиксирующее непосредственно логические противоречия из-за ошибок в кодировании
 */
public class SimulationSystemException extends SimulationException{
    public SimulationSystemException() {
    }

    public SimulationSystemException(String s) {
        super(s);
    }

    public SimulationSystemException(String s, Throwable throwable) {
        super(s, throwable);
    }

    public SimulationSystemException(Throwable throwable) {
        super(throwable);
    }

    public SimulationSystemException(String s, Throwable throwable, boolean b, boolean b1) {
        super(s, throwable, b, b1);
    }
}
