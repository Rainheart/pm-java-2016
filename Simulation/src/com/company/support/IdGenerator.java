package com.company.support;

/**
 * Created by Tisov-VV on 22.10.2016.
 * Генератор целочисленных идентификаторов
 */
public class IdGenerator {

    long currentValue;

    public IdGenerator() {
        currentValue = 0L;
    }

    public long generate() {
        return currentValue++;
    }
}
