package com.company.support;

/**
 * Created by Tisov-VV on 22.10.2016.
 * Интерфейс для объектов, которые можно идентифицировать целым числом
 */
public interface Identifiable {
    public Long getId();
}
