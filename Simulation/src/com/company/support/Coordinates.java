package com.company.support;

/**
 * Created by Tisov-VV on 22.10.2016.
 * Сущность координат для нашего пространства
 */
public class Coordinates implements Comparable {

    private Long x;
    private Long y;

    public Coordinates(Long x, Long y) {
        this.x = x;
        this.y = y;
    }

    public Long getX() {
        return x;
    }

    public Long getY() {
        return y;
    }

    @Override
    public boolean equals(Object o) {
        return this.compareTo(o) == 0;
    }

    @Override
    public int compareTo(Object o) {
        if (o == null) {
            return 1;
        }
        if (this == o) {
            return 0;
        }

        if (o instanceof Coordinates) {
            Coordinates second = (Coordinates) o;
            if (x == null) {
                return -1;
            }
            else {
                int result = this.x.compareTo(second.getX());
                if (result == 0) {
                    return this.y.compareTo(second.getY());
                }
                else {
                    return result;
                }
            }
        }
        return 1;
    }

    public String toString() {
        return "Coordinates: " + x + ", " + y;
    }
}
