package com.company.support;

/**
 * Created by Stanislav on 28.10.2016.
 * Интерфейс для объектов, которые можно именовать по типу и идентификатору
 */

public interface Nameable extends Identifiable {
    public String getName();
}
