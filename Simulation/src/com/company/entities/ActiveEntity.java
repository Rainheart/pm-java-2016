package com.company.entities;

import com.company.entities.needs.Need;
import com.company.entities.needs.NeedType;
import com.company.processor.messages.requests.ActionRequest;
import com.company.processor.messages.responses.ActionResponse;
import com.company.support.Identifiable;

/**
 * Created by Tisov-VV on 22.10.2016.
 * Интерфейс сущности, способной осуществлять какие-либо действия
 */
public interface ActiveEntity extends Entity, Identifiable {

    /**
     * Метод проработки следующего действия
     * @return - запрос на действие, исполняемый процессором симуляции
     */
    public ActionRequest act();

    /**
     * Метод обработки овтета от процессора
     * @param response - ответ на запрос о действии
     */
    public void listenResponse(ActionResponse response);

    public void addNeed(Need need);

    public void aggravateNeeds();

    public Need getNeedByType(NeedType type);
}
