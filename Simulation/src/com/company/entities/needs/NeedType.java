package com.company.entities.needs;

/*
    Created by Stanislav on 14.11.2016
*/

public enum NeedType {

    WAIT,
    MOVE;

}
