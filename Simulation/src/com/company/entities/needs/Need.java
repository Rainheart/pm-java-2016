package com.company.entities.needs;

/**
 * Created by fpm13bse on 05.12.16.
 */
public class Need {

    private NeedType type;
    private int increment;
    private int decrement;
    private int value;

    public Need(NeedType type, int value) {
        this.type = type;
        this.value = value;
    }

    public Need(NeedType type, int value, int increment, int decrement) {
        this.type = type;
        this.increment = increment;
        this.decrement = decrement;
        this.value = value;
    }

    public NeedType getType() {
        return type;
    }

    public int getValue() {
        return value;
    }

    public void inc() {
        value += increment;
    }

    public void dec() {
        value -= decrement;
    }

}
