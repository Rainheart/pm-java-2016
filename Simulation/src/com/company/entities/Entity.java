package com.company.entities;

import com.company.field.Cell;
import com.company.support.Nameable;

/**
 * Created by Tisov-VV on 22.10.2016.
 * Интерфейс сущности располагаемой в пространстве симулятора
 */
public interface Entity extends Nameable {

    public void setCell(Cell cell);
    public Cell getCell();
}
