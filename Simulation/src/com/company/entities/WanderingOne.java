package com.company.entities;

import com.company.entities.needs.Need;
import com.company.entities.needs.NeedType;
import com.company.processor.messages.MoveDirection;
import com.company.processor.messages.requests.ActionRequest;
import com.company.processor.messages.requests.MoveActionRequest;
import com.company.processor.messages.requests.WaitActionRequest;
import com.company.processor.messages.responses.ActionResponse;
import com.company.processor.messages.responses.ActionResponseState;

import java.util.HashMap;
import java.util.Map;
import java.util.Random;

/**
 * Created by Tisov-VV on 22.10.2016.
 * Странник. Тестовый объект, который умеет просто блуждать туда-сюда
 */
public class WanderingOne extends ActiveEntityBase {

    private static int NUMBER_OF_MOVEMENT_DECISIONS = MoveDirection.values().length;
    private static int NUMBER_OF_WAITING_DECISIONS = MoveDirection.values().length;

    private ActionRequest request;
    private Map<Class<? extends ActionRequest>, NeedType> actionAndNeeds;

    public WanderingOne(Long id) {
        super(id);
        Random random = new Random();
        addNeed(new Need(NeedType.MOVE, random.nextInt(100), random.nextInt(10), random.nextInt(10)));
        addNeed(new Need(NeedType.WAIT, random.nextInt(100), random.nextInt(10), random.nextInt(10)));
        actionAndNeeds = new HashMap<>();
        actionAndNeeds.put(MoveActionRequest.class, NeedType.MOVE);
        actionAndNeeds.put(WaitActionRequest.class, NeedType.WAIT);
    }

    @Override
    public ActionRequest act() {
        Need req = getMainNeed();
        aggravateNeeds();
        if (req.getType() == NeedType.MOVE) {
            Random random = new Random();
            int decision = random.nextInt(NUMBER_OF_MOVEMENT_DECISIONS + NUMBER_OF_WAITING_DECISIONS);
            int directionNumber = decision % NUMBER_OF_MOVEMENT_DECISIONS;
            MoveDirection direction = MoveDirection.values()[directionNumber];
            request = new MoveActionRequest(generator.generate(), direction);
            return request;
        } else {
            request = new WaitActionRequest(generator.generate());
            return request;
        }
    }

    @Override
    public void listenResponse(ActionResponse response) {
        //На пришедшие ответы никак не реагируем
        if (response.getId() == request.getId() && response.getState() == ActionResponseState.SUCCESSFUL) {
            Need need = getNeedByType(actionAndNeeds.get(request.getClass()));
            need.dec();
        } else {
            System.out.println(this.getName() + ". Некорректный идентификатор ответа.");
            System.out.println("Последний запрос: " + request.getId() + ", ответ: " + response.getId());
        }
    }

}
