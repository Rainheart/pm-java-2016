package com.company.entities;

/**
 * Смертный Странник.
 */
public class MortalWanderingOne extends WanderingOne implements MortalEntity {

    private boolean isAlive;

    public MortalWanderingOne(Long id) {
        super(id);
        isAlive = true;
    }

    @Override
    public boolean isAlive() {
        return isAlive;
    }

    @Override
    public void die() {
        isAlive = false;
    }
}
