package com.company.entities;

import com.company.entities.needs.Need;
import com.company.entities.needs.NeedType;
import com.company.field.Cell;
import com.company.support.IdGenerator;

import java.util.*;

/**
 * Created by Tisov-VV on 22.10.2016.
 * Базовый класс для исполнителей интерфейса ActiveEntity
 */
public abstract class ActiveEntityBase extends EntityBase implements ActiveEntity {

    private List<Need> needs;

    //Ссылка на занимаемую клетку
    protected Cell cell;
    //Генератор идентификаторя для запросов к процессору
    protected IdGenerator generator = new IdGenerator();

    public ActiveEntityBase(Long id)
    {
        super(id);
        needs = new LinkedList<>();
    }

    @Override
    public void addNeed(Need need) {
        needs.add(need);
    }

    @Override
    public void aggravateNeeds() {
        for (Need need : needs) {
            need.inc();
        }
    }

    @Override
    public Need getNeedByType(NeedType type) {
        for(Need need : needs) {
            if (need.getType() == type) {
                return need;
            }
        }
        return null;
    }

    protected Need getMainNeed() {
        int max = Integer.MIN_VALUE;
        Need maxNeed = null;
        for(Need need : needs) {
            if (need.getValue() >= max) {
                max = need.getValue();
                maxNeed = need;
            }
        }
        return maxNeed;
    }

    public IdGenerator getGenerator() {
        return generator;
    }

    public void setGenerator(IdGenerator generator) {
        this.generator = generator;
    }

    @Override
    public Cell getCell() {
        return cell;
    }

    @Override
    public void setCell(Cell cell) {
        this.cell = cell;
    }

}
