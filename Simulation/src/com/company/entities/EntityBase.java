package com.company.entities;

/**
 * Created by Stanislav on 28.10.2016.
 * Базовый класс для всех сущностей
 */

public abstract class EntityBase implements Entity {

    private Long id;

    public EntityBase(Long id) {
        this.id = id;
    }

    @Override
    public final String getName() {
        return "тип: " + this.getClass().getTypeName() + ", идентификатор: " + id;
    }

    @Override
    public final Long getId() {
        return id;
    }
}
