package com.company.entities;

/**
 * Интерфейс сущности "Смертный"
 */
public interface MortalEntity extends ActiveEntity {

    public boolean isAlive();
    public void die();

}
