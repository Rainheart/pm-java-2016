package com.company.processor;

import com.company.entities.ActiveEntity;
import com.company.entities.Entity;
import com.company.exceptions.*;
import com.company.field.Cell;
import com.company.field.Space;
import com.company.processor.handlers.ActionRequestHandler;
import com.company.processor.handlers.HandlersFactory;
import com.company.processor.messages.requests.ActionRequest;
import com.company.processor.messages.responses.ActionResponse;
import com.company.processor.messages.responses.ActionResponseState;
import com.company.support.Coordinates;

import java.util.HashSet;
import java.util.Set;

/**
 * Created by Tisov-VV on 22.10.2016.
 * Обработчик симуляции.
 * Сущность, которая создаёт симуляцию и управляет её составными частями
 */
public abstract class SimulationProcessor {

    //Пространство симуляции
    protected Space space;
    //Объекты симуляции, не обладающие возможностью действовать
    private Set<Entity> entities = new HashSet<>();
    //Объекты симуляции, способные совершать действия
    private Set<ActiveEntity> activeEntities = new HashSet<>();
    //Умершие объекты симуляции
    private Set<ActiveEntity> deadEntities = new HashSet<>();

    protected SimulationProcessor() throws SimulationSystemException, OutOfSpacePlacementException {
        this.space = initializeSpace();
        populate();
    }

    public Set<ActiveEntity> getActiveEntities() {
        return activeEntities;
    }

    public Set<ActiveEntity> getDeadEntities() {
        return deadEntities;
    }

    /**
     * Инициализация пространства
     * @return
     */
    protected abstract Space initializeSpace();

    /**
     * Заполнение пространства объектами
     * @throws SimulationSystemException
     * @throws OutOfSpacePlacementException
     */
    protected abstract void populate() throws SimulationSystemException, OutOfSpacePlacementException;

    protected void registerEntity(Entity entity, Coordinates coordinates) throws OutOfSpacePlacementException {
        placeInSpace(entity, coordinates);
        entities.add(entity);
        log(entity);
    }

    protected void registerEntity(ActiveEntity entity, Coordinates coordinates) throws OutOfSpacePlacementException {
        placeInSpace(entity, coordinates);
        activeEntities.add(entity);
        log(entity);
    }

    private void log(Entity entity) {
        Long x = entity.getCell().getCoordinates().getX();
        Long y = entity.getCell().getCoordinates().getY();
        System.out.println(entity.getName() + " зарегистрирован на: " + x + ", " + y);
    }

    private void placeInSpace(Entity entity, Coordinates coordinates) throws OutOfSpacePlacementException {
        Cell cell = space.get(coordinates);
        if (cell == null) {
            throw new OutOfSpacePlacementException("Клетка с координатами " + coordinates + " не доступна");
        }

        entity.setCell(cell);
        cell.addEntity(entity);
    }

    /**
     * Проведение симуляции одного временного кванта
     */
    public void processQuantum() {

        for (ActiveEntity activeEntity: activeEntities)
        {
            ActionRequest request;
            try {
                //Для каждого участника подхватываем его запрос
                request = activeEntity.act();
            }
            catch (Exception e) {
                //Если произошло какое-то исключение - уведомляем объект о том, что его прошлое действие провалилось
                //и переходим к следующей итерации цикла
                activeEntity.listenResponse(new ActionResponse(ActionResponseState.ERROR, null));
                e.printStackTrace();
                continue;
            }

            //Подписываем сообщение за его отправителя
            request.setSender(activeEntity);
            ActionRequestHandler handler;
            ActionResponse response;
            try {
                //Ищем соответствующего обработчика запроса и производим обработку
                handler = getHandler(request);
                response = handler.handle(request, space);
            } catch (ActionMessageException e) {
                ExceptionHandler<ActiveEntity> exceptionHandler = ExceptionHandlersFactory.getHandler(e);
                exceptionHandler.handle(e, request, activeEntity);
                continue;
            }

            //Направляем ответ объекту
            activeEntity.listenResponse(response);
        }
        for (ActiveEntity entity: deadEntities) {
            activeEntities.remove(entity);
        }
    }

    private ActionRequestHandler getHandler(ActionRequest request) throws UnknownActionRequestException {
        return HandlersFactory.getHandler(request);
    }

}
