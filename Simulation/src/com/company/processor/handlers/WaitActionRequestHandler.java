package com.company.processor.handlers;

import com.company.exceptions.RequestHandlerTypeMismatchException;
import com.company.field.Space;
import com.company.processor.messages.requests.ActionRequest;
import com.company.processor.messages.requests.WaitActionRequest;
import com.company.processor.messages.responses.ActionResponse;
import com.company.processor.messages.responses.ActionResponseState;

/**
 * Created by Tisov-VV on 22.10.2016.
 * Обработчик ожидания
 */
public class WaitActionRequestHandler implements ActionRequestHandler {

    @Override
    public ActionResponse handle(ActionRequest request, Space space) throws RequestHandlerTypeMismatchException {
        WaitActionRequest req = (WaitActionRequest) request;

        log(req);
        return new ActionResponse(ActionResponseState.SUCCESSFUL, req.getId());
    }

    private void log(WaitActionRequest request) {
        System.out.println(request.getSender().getName() + " решил подождать.");
    }
}
