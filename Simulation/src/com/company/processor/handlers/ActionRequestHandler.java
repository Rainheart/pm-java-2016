package com.company.processor.handlers;

import com.company.exceptions.EntityDieException;
import com.company.exceptions.RequestHandlerTypeMismatchException;
import com.company.exceptions.UnreachableCellException;
import com.company.field.Space;
import com.company.processor.messages.requests.ActionRequest;
import com.company.processor.messages.responses.ActionResponse;

/**
 * Created by Tisov-VV on 22.10.2016.
 * Интерфейса обработчика запросов на действие.
 * Принимает на вход запрос от объекта симуляции, меняет общее состояние системы и возвращает ответ-уведомление об изменениях.
 * Только обработчик должен быть наделён непосредственным правом на изменение состояния пространства симуляции.
 */
public interface ActionRequestHandler {

    public ActionResponse handle(ActionRequest request, Space space) throws RequestHandlerTypeMismatchException, UnreachableCellException, EntityDieException;
}
