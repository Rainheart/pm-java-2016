package com.company.processor.handlers;

import com.company.entities.ActiveEntity;
import com.company.entities.MortalWanderingOne;
import com.company.exceptions.EntityDieException;
import com.company.exceptions.RequestHandlerTypeMismatchException;
import com.company.exceptions.UnreachableCellException;
import com.company.field.Cell;
import com.company.field.Space;
import com.company.processor.messages.MoveDirection;
import com.company.processor.messages.requests.ActionRequest;
import com.company.processor.messages.requests.MoveActionRequest;
import com.company.processor.messages.responses.ActionResponse;
import com.company.processor.messages.responses.ActionResponseState;
import com.company.support.Coordinates;

/**
 * Created by Tisov-VV on 22.10.2016.
 * Обработка запроса на перемещение в пространстве
 */
public class MoveActionRequestHandler implements ActionRequestHandler {

    @Override
    public ActionResponse handle(ActionRequest request, Space space) throws RequestHandlerTypeMismatchException, UnreachableCellException, EntityDieException {
        MoveActionRequest moveActionRequest = (MoveActionRequest) request;

        //Смотрим перемещение из запроса и вычисляем координаты назначения
        MoveDirection direction = moveActionRequest.getDirection();
        ActiveEntity entity = moveActionRequest.getSender();
        Cell currentCell = entity.getCell();
        long destinationX = currentCell.getCoordinates().getX() + direction.getX();
        long destinationY = currentCell.getCoordinates().getY() + direction.getY();

        Cell destination = space.get(new Coordinates(destinationX, destinationY));

        //Если в пространстве клетка с такими координатами отсутствует - бросаем исключение обработки
        if (destination == null) {
            throw new UnreachableCellException();
        }

        if (!destination.isFree() && entity instanceof MortalWanderingOne) {
            //Смертный странник умирает
            throw new EntityDieException();
        }
        else {
            //Перемещаем объект
            currentCell.removeEntity(entity);
            destination.addEntity(entity);
            log(moveActionRequest);
            //Возвращаем ответ об успешном действии
            return new ActionResponse(ActionResponseState.SUCCESSFUL, request.getId());
        }
    }

    private void log(MoveActionRequest request) {
        ActiveEntity entity = request.getSender();
        System.out.println(entity.getName() + " перешёл на клетку " + entity.getCell().getCoordinates().getX() + ", " + entity.getCell().getCoordinates().getY());
    }
}
