package com.company.processor.handlers;

import com.company.exceptions.UnknownActionRequestException;
import com.company.processor.messages.requests.ActionRequest;
import com.company.processor.messages.requests.MoveActionRequest;
import com.company.processor.messages.requests.WaitActionRequest;

import java.util.HashMap;
import java.util.Map;
import java.util.TreeMap;

/**
 * Created by Tisov-VV on 22.10.2016.
 * Фабрика обработчиков запросов на действие.
 * Здесь фиксируется логика соответствия между запросами и обработчиками
 */
public class HandlersFactory {

    private static Map<Class<? extends ActionRequest>, ActionRequestHandler> factoryMap = new HashMap<>();

    static {
        factoryMap.put(MoveActionRequest.class, new MoveActionRequestHandler());
        factoryMap.put(WaitActionRequest.class, new WaitActionRequestHandler());
    }

    public static ActionRequestHandler getHandler(ActionRequest request) throws UnknownActionRequestException {
        ActionRequestHandler handler = factoryMap.get(request.getClass());

        if (handler == null) {
            throw new UnknownActionRequestException("Неизвестный запрос - " + request.getClass());
        }

        return handler;
    }
}
