package com.company.processor.messages.responses;

/**
 * Created by Tisov-VV on 22.10.2016.
 * Статус выполнения запроса на действие
 */
public enum ActionResponseState {
    //Запрос успешно выполнен
    SUCCESSFUL,
    //Запрос привел к смерти объекта
    DIE,
    //Запрос выполнен с ошибкой
    ERROR
}
