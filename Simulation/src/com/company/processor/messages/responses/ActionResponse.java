package com.company.processor.messages.responses;

import com.company.support.Identifiable;

/**
 * Created by Tisov-VV on 22.10.2016.
 * Ответ об обработке запроса на действие.
 * Формируется обработчиком с целью уведомления отправителя исходного запроса.
 */
public class ActionResponse implements Identifiable{

    //Идентификатор запроса
    Long id;
    //Статус выполнения запроса
    ActionResponseState state;

    public ActionResponse(ActionResponseState state, Long id) {
        this.id = id;
        this.state = state;
    }

    @Override
    public Long getId() {
        return this.id;
    }

    public ActionResponseState getState() {
        return state;
    }
}
