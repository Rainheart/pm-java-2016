package com.company.processor.messages;

/**
 * Created by Tisov-VV on 22.10.2016.
 * Направление движения
 */
public enum  MoveDirection {

    UP(0, 1),
    RIGHT(1, 0),
    DOWN(0, -1),
    LEFT(-1, 0);

    long x,y;

    private MoveDirection(long x, long y) {
        this.x = x;
        this.y = y;
    }

    public long getX() {
        return x;
    }

    public long getY() {
        return y;
    }
}
