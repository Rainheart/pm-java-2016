package com.company.processor.messages.requests;

/**
 * Created by Tisov-VV on 22.10.2016.
 * Запрос об ожидании (участник симуляции решил ничего не делать)
 */
public class WaitActionRequest extends ActionRequestBase {
    public WaitActionRequest(long id) {
        super(id);
    }
}
