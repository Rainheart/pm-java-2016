package com.company.processor.messages.requests;

import com.company.processor.messages.MoveDirection;

/**
 * Created by Tisov-VV on 22.10.2016.
 * Запрос на передвижение на соседнюю клетку
 */
public class MoveActionRequest extends ActionRequestBase {

    //Направление движения
    private MoveDirection direction;

    public MoveActionRequest(long id, MoveDirection direction) {
        super(id);
        this.direction = direction;
    }

    public MoveDirection getDirection() {
        return direction;
    }
}
