package com.company.processor.messages.requests;

import com.company.entities.ActiveEntity;
import com.company.support.Identifiable;

/**
 * Created by Tisov-VV on 22.10.2016.
 * Интерфейс запроса на действие.
 * Посылается ActivityEntity к процессору симулятора с целью изменить состояние симуляции
 */
public interface ActionRequest extends Identifiable{

    /**
     * Получение отправителя запроса
     * @return
     */
    public ActiveEntity getSender();

    public void setSender(ActiveEntity sender);
}
