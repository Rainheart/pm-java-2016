package com.company.processor.messages.requests;

import com.company.entities.ActiveEntity;

/**
 * Created by Tisov-VV on 22.10.2016.
 * Базовый класс для запроса на действие
 */
public abstract class ActionRequestBase implements ActionRequest {

    //Идендификатор запроса
    protected long id;
    //Отправитель запроса
    protected ActiveEntity sender;

    public ActionRequestBase(long id) {
        this.id = id;
    }

    @Override
    public Long getId() {
        return id;
    }

    @Override
    public ActiveEntity getSender() {
        return sender;
    }

    public void setSender(ActiveEntity sender) {
        this.sender = sender;
    }
}
