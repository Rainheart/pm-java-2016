package com.company.processor;

import com.company.entities.MortalWanderingOne;
import com.company.entities.WanderingOne;
import com.company.exceptions.OutOfSpacePlacementException;
import com.company.exceptions.SimulationSystemException;
import com.company.field.Cell;
import com.company.field.Space;
import com.company.support.Coordinates;
import com.company.support.IdGenerator;

import java.util.Random;

/**
 * Created by Tisov-VV on 23.10.2016.
 * Тестовая реализация процессора симуляции,
 * Создаём квадратное пространство и случайным образом заполняем его объектами.
 */
public class SimulationProcessorImplementation extends SimulationProcessor {

    private IdGenerator generator;
    private static final long SPACE_WIDTH = 100;
    private static final long SPACE_HEIGHT = 100;

    private static final long WANDERING_ONES_NUMBER = 10;
    private static final long MORTAL_WANDERING_ONES_NUMBER = 10;


    public SimulationProcessorImplementation() throws SimulationSystemException, OutOfSpacePlacementException {
        super();
    }

    @Override
    protected Space initializeSpace() {

        return new Space(SPACE_WIDTH, SPACE_HEIGHT);
    }

    @Override
    protected void populate() throws SimulationSystemException, OutOfSpacePlacementException {
        generator = new IdGenerator();
        Random random = new Random();

        for (int i = 0; i < MORTAL_WANDERING_ONES_NUMBER; i++) {
            registerEntity(new MortalWanderingOne(generator.generate()), space.getRandomCoordinates(random.nextInt()));
        }

        for (int i = 0; i < WANDERING_ONES_NUMBER; i++) {
            registerEntity(new WanderingOne(generator.generate()), space.getRandomCoordinates(random.nextInt()));
        }
    }
}
