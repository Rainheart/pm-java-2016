package com.company;

import com.company.exceptions.SimulationException;
import com.company.processor.SimulationProcessor;
import com.company.processor.SimulationProcessorImplementation;

import java.util.concurrent.TimeUnit;

public class Main {

    //Время на выполнение симуляции одного временного кванта (в милисекундах)
    private static final int TIME_QUANTUM = 1000;
    //Число временных квантов, после которых симуляция завершается
    private static final int PROCESSOR_LIFE_TIME = 10;

    private static SimulationProcessor processor;

    public static SimulationProcessor getProcessor() {
        return processor;
    }

    public static void main(String[] args) {
        processor = null;
        boolean active = true;
        try {
            processor = new SimulationProcessorImplementation();
        } catch (SimulationException e) {
            e.printStackTrace();
            active = false;
        }

        int remainingTime = PROCESSOR_LIFE_TIME;
        while (active)
        {
            processor.processQuantum();
            try {
                TimeUnit.MILLISECONDS.sleep(TIME_QUANTUM);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            remainingTime --;
            active = active && remainingTime != 0;
        }

    }
}
