package com.company.field;

import com.company.entities.Entity;
import com.company.support.Coordinates;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Tisov-VV on 22.10.2016.
 * Сущность клетки, атомарной дискретной единицы пространства симуляции
 */
public class Cell {

    //Пространство, к которому данная клетка принадлежит
    private Space space;
    //Координаты данной клетки
    private Coordinates coordinates;
    //Объекты, находящиеся в данной клетке
    private List<Entity> objects = new ArrayList<Entity>();

    public Cell(Space space, Coordinates coordinates) {
        this.space = space;
        this.coordinates = coordinates;
    }

    public Coordinates getCoordinates() {
        return coordinates;
    }

    public boolean isFree() {
        return objects.isEmpty();
    }

    /**
     * Добавить объект на данную клетку
     * @param entity
     */
    public void addEntity(Entity entity) {
        objects.add(entity);
        entity.setCell(this);
    }

    /**
     * Убрать объект с данной клетки
     * @param entity
     */
    public void removeEntity(Entity entity) {
        objects.remove(entity);
    }


}
