package com.company.field;

import com.company.exceptions.SimulationSystemException;
import com.company.support.Coordinates;
import javafx.util.Pair;

import java.util.List;
import java.util.Map;
import java.util.TreeMap;

/**
 * Created by Tisov-VV on 22.10.2016.
 * Географическое пространство симуляции.
 */
public class Space {

    private TreeMap<Coordinates, Cell> map;

    /**
     * Конструктор для создания прямоугольного пространства
     * @param n - протяжённость пространства по оси OX
     * @param m - протяжённость пространства по оси OY
     */
    public Space(long n, long m) {
        map = new TreeMap<>();
        for (long i = 0; i < n; i++) {
            for (long j =0; j < m; j++)
            {
                Coordinates coordinates = new Coordinates(i, j);
                map.put(coordinates, new Cell(this, coordinates));
            }
        }
    }

    /**
     * Получение координат по некоторому числу.
     * Переданное значение не обязано являться строгим идентификатором для получаемых координат.
     * Для "случайности" координат необходимо передать в аргумент случайное число с нужным распределением.
     * @param rand
     * @return
     * @throws SimulationSystemException - если выбросилось, значит имеется ошибка в логике работы метода
     */
    public Coordinates getRandomCoordinates(long rand) throws SimulationSystemException {
        long index = Math.abs(rand) % map.size();

        for (Coordinates key: map.keySet())
        {
            if (index == 0) {
                return key;
            }
            index--;
        }
        throw new SimulationSystemException("Не удалось получить случайные координаты по коду " + rand);
    }

    /**
     *  Получение клетки по указанным координатам
     * @param coordinates
     * @return
     */
    public Cell get(Coordinates coordinates) {
        return map.get(coordinates);
    }

}
